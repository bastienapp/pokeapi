import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import PokemonList from "./pages/PokemonList/PokemonList";
import PokemonDetails from "./pages/PokemonDetails/PokemonDetails";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <PokemonList />,
  },
  {
    // déclare la route et prend un paramètre pokeId automatiquement (qui sera récupéré par useParams)
    path: "/pokemon/:pokeId",
    element: <PokemonDetails />
  }
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
