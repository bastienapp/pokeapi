import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";

function PokemonList() {

  const [pokemonList, setPokemonList] = useState([]);
  /*
  pokemonList: [
    {
      entry_number: 1,
      pokemon_species: {
        name: "bulbasaur",
        url: "https://pokeapi.co/api/v2/pokemon-species/1/",
      },
    },
    ...
  ]
  */

  useEffect(() => {
    axios.get("https://pokeapi.co/api/v2/pokedex/2")
      .then((response) => setPokemonList(response?.data?.pokemon_entries));
  }, [])

  return (
    <ul>
      {pokemonList.map((eachPokemon) => (
        <li key={eachPokemon?.entry_number}>
          <Link to={`/pokemon/${eachPokemon?.entry_number}`}>{eachPokemon?.pokemon_species?.name}</Link>
        </li>
      ))}
    </ul>
  );
}

export default PokemonList;
