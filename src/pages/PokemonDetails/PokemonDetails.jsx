import axios from "axios";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

function PokemonDetails() {
  // récupère le paramètre de l'url qui a été déclaré dans le router
  const { pokeId } = useParams();
  // https://pokeapi.co/api/v2/pokemon/1
  const [pokemon, setPokemon] = useState(null);

  useEffect(() => {
    // appel d'API pour récupérer le pokémon grace à son identifiant
    const API_URL = `https://pokeapi.co/api/v2/pokemon/${pokeId}`;

    axios.get(API_URL).then((response) => {
      /*
        {
          name: "bulbasaur",
          sprites: {
            front_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
          }
        }
        */
      setPokemon(response.data);
    });
  }, []); // au montage du composant

  return (
    <div>
      {pokemon !== null ? (
        <>
          <h1>{pokemon?.name}</h1>
          <img src={pokemon?.sprites?.front_default} alt={pokemon?.name} />
        </>
      ) : (
        "Chargement du Pokémon"
      )}
      <div>
        <Link to="/">Back to Pokémon list</Link>
      </div>
    </div>
  );
}

export default PokemonDetails;
